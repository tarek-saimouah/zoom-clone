# Zoom Clone API

Zoom Clone REST API documentation.

## Contents

- [User Routes](#user-routes)

  - [Register user](#register-user)
  - [Login user](#login-user)

- [Files Routes](#files-routes)

  - [Upload meeting file](#upload-meeting-file)
  - [Download meeting file](#download-meeting-file)

- [Meeting Routes](#meeting-routes)

  - [Create meeting](#create-meeting)
  - [Add user to meeting](#add-user-to-meeting)

---

## User Routes

#### Register User

- **Request**

  **`POST`** `/auth/signup`

- **Body**

  ```
  email: String, pattern(email), *required.
  password: String, min(8), *required.
  ```

- **Responses**

  - **Created**

    Status: **`201`**

    ```
    {
      "token": String
    }
    ```

  - **Forbidden**

    Status: **`403`**

    ```
    {
      "statusCode": 403,
      "message": "email is already registered.",
      "error": "Forbidden"
    }
    ```

#### Login User

- **Request**

  **`POST`** `/auth/signin`

- **Body**

  ```
  phoneNumber: String, *required.
  password: String, *required.
  ```

- **Responses**

  - **Created**

    Status: **`201`**

    ```
    {
      "token": String
    }
    ```

  - **Forbidden**

    Status: **`403`**

    ```
    {
      "statusCode": 403,
      "message": "Wrong credentials",
      "error": "Forbidden"
    }
    ```

---

## Files Routes

#### Upload Meeting File

- **Request**

  **`POST`** `/files/upload/:meetingId`

- **Headers**

  > `'Content-Type': "multipart/form-data"` > `'Authorization': "Bearer ${JwtToken}"`

- **Body**

  ```
  file : valid(mp4|mp3), maxCount(1), *required.
  ```

- **Responses**

  - **Created**

    Status: **`201`**

    ```
    {
      "success": true,
      "msg": "File metadata saved successfully"
    }
    ```

  - **Bad Request**

    Status: **`400`**

    ```
    {
      "statusCode": 400,
      "message": "Validation failed (expected type is .(mp4|mp3))",
      "error": "Bad Request"
    }
    ```

#### Download Meeting File

- **Request**

  **`GET`** `/files/download/:meetingId`

- **Headers**

  > `'Authorization': "Bearer ${JwtToken}"`

---

## Meeting Routes

#### Create Meeting

- **Request**

  **`POST`** `/meeting`

- **Headers**

  > `'Authorization': "Bearer ${JwtToken}"`

- **Responses**

  - **Created**

    Status: **`201`**

    ```
    {
      "meetingId": Int
    }
    ```

#### Add User To Meeting

- **Request**

  **`PATCH`** `/meeting/:meetingId/add-user`

- **Headers**

  > `'Authorization': "Bearer ${JwtToken}"`

- **Responses**

  - **Created**

    Status: **`201`**

    ```
    {
      "meetingId": Int
    }
    ```
