import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { AuthDto } from './dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService,
  ) {}

  async signup(dto: AuthDto) {
    try {
      const exists = await this.prisma.user.findFirst({
        where: {
          email: dto.email,
        },
      });

      if (exists) {
        return new ForbiddenException('email is already registered.');
      }

      const hash = await bcrypt.hash(dto.password, 10);

      const user = await this.prisma.user.create({
        data: {
          email: dto.email,
          hash,
        },
      });

      const token = this.signToken(user.id, user.email);

      return { token };
    } catch (err) {
      throw err;
    }
  }

  async signin(dto: AuthDto) {
    try {
      const user = await this.prisma.user.findFirst({
        where: {
          email: dto.email,
        },
      });

      if (!user) {
        return new ForbiddenException('wrong credentials.');
      }

      const match = await bcrypt.compare(user.hash, dto.password);

      if (!match) {
        return new ForbiddenException('wrong credentials.');
      }

      const token = this.signToken(user.id, user.email);

      return { token };
    } catch (err) {
      throw err;
    }
  }

  private signToken(userId: number, email: string): Promise<string> {
    const payload = {
      sub: userId,
      email,
    };

    return this.jwt.signAsync(payload, {
      expiresIn: '30d',
      secret: this.config.get('JWT_SECRET'),
    });
  }
}
