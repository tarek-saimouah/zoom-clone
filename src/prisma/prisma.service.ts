import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient {
  constructor(private config: ConfigService) {
    super({
      datasources: {
        db: {
          url: `postgresql://${config.get('DB_USER')}:${config.get(
            'DB_PASS',
          )}@${config.get('DB_HOST')}:5434/zoomdb?schema=public`,
        },
      },
    });
  }
}
