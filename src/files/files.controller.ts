import {
  Controller,
  FileTypeValidator,
  Get,
  Param,
  ParseFilePipe,
  Post,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { Request, Response } from 'express';
import { createReadStream } from 'fs';
import { diskStorage } from 'multer';
import { FilesService } from './files.service';

@Controller('files')
export class FilesController {
  constructor(
    private filesService: FilesService,
    private config: ConfigService,
  ) {}

  @Post('upload/:meetingId')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: '/var/www/zoom.com/files',
      }),
    }),
  )
  async uploadFile(
    @UploadedFile(
      new ParseFilePipe({
        validators: [new FileTypeValidator({ fileType: '.(mp4|mp3)' })],
      }),
    )
    file: Express.Multer.File,
    @Param() params,
  ) {
    return await this.filesService.saveFileMetadata(
      file.path,
      file.mimetype,
      +params.meetingId,
    );
  }

  @Get('download/:meetingId')
  @UseGuards(AuthGuard('jwt'))
  async downloadFile(
    @Res() res: Response,
    @Req() req: Request,
    @Param() params,
  ) {
    const userId = req.user.sub;
    const meetingId = params.meetingId;

    const data = await this.filesService.getFileMetadata(+userId, +meetingId);

    const file = createReadStream(data.filePath);
    file.pipe(res);
  }
}
