import {
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class FilesService {
  constructor(private prisma: PrismaService) {}

  async saveFileMetadata(
    filePath: string,
    fileType: string,
    meetingId: number,
  ) {
    try {
      const meeting = await this.prisma.meeting.findUnique({
        where: {
          id: meetingId,
        },
      });

      if (!meeting) {
        return new NotFoundException('meeting not found.');
      }

      await this.prisma.meeting.update(
        {
          where: { id: meetingId },
        },
        { filePath, fileType },
      );

      return { success: true, msg: 'File metadata saved successfully' };
    } catch (err) {
      console.log(err.message);

      return new InternalServerErrorException('Somthing went wrong.');
    }
  }

  async getFileMetadata(userId: number, meetingId: number) {
    try {
      // check if user had a relation with meeting
      const relation = await this.prisma.meetingsOnUsers.findFirst({
        where: { userId, meetingId },
      });

      if (!relation) return new UnauthorizedException();

      const meeting = await this.prisma.meeting.findUnique({
        where: {
          id: meetingId,
        },
      });

      if (!meeting) {
        return new NotFoundException('meeting not found.');
      }

      return { filePath: meeting.filePath };
    } catch (err) {
      console.log(err.message);

      return new InternalServerErrorException('Somthing went wrong.');
    }
  }
}
