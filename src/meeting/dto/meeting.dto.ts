import { IsNumber } from 'class-validator';

export class MeetingDto {
  @IsNumber()
  userId: number;
}
