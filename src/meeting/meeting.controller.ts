import { Controller, Param, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { MeetingService } from './meeting.service';

@Controller('meeting')
export class MeetingController {
  constructor(private meetingService: MeetingService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  async createMeeting(@Req() req: Request) {
    const userId = req.user.sub;

    const meeting = await this.meetingService.createMeetingRecord(+userId);

    return { success: true, meeitngId: meeting.id };
  }

  @Patch('/:meetingId/add-user')
  @UseGuards(AuthGuard('jwt'))
  async addUserToMeeting(@Req() req: Request, @Param() params) {
    const userId = req.user.sub;
    const meetingId = params.meeitngId;

    const meeting = await this.meetingService.addUserToMeetingRecord(
      +meetingId,
      +userId,
    );

    return { success: true, meeitngId: meeting.id };
  }
}
