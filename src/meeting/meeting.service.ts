import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class MeetingService {
  constructor(private prisma: PrismaService) {}

  async createMeetingRecord(userId: number) {
    try {
      const meeting = await this.prisma.meeting.create({
        data: { createdBy: userId },
      });

      const relation = await this.prisma.meetingsOnUsers.create({
        data: { userId, meetingId: meeting.id },
      });

      return meeting;
    } catch (err) {
      return new InternalServerErrorException();
    }
  }

  async addUserToMeetingRecord(meetingId: number, userId: number) {
    try {
      const meeting = await this.prisma.meeting.findUnique({
        where: { id: meetingId },
      });

      if (!meeting) {
        return new NotFoundException('meeting not found');
      }

      const relation = await this.prisma.meetingsOnUsers.create({
        data: { userId, meetingId },
      });

      return meeting;
    } catch (err) {
      return new InternalServerErrorException();
    }
  }
}
